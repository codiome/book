#!/usr/bin/env python

import os, requests, json

s = requests.Session()
s.mount('https://', requests.adapters.HTTPAdapter(max_retries=3))
r = s.get("{0}&grade={1}".format(os.environ['CODIO_AUTOGRADE_URL'], 67.89))
print r.content

parsed = json.loads(r.content)

exit( 0 if parsed['code'] == 1 else 1)