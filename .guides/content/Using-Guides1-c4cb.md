


![.guides/img/ffc](.guides/img/ffc.bmp)
![.guides/img/ffc](.guides/img/ffc.gif)
![.guides/img/ffc](.guides/img/ffc.jpg)
![.guides/img/ffc](.guides/img/ffc.png)

## Available style
The later sections in this Guide show you the available markdown commands for Guides.

## Starting Guides

Codio Guides has 2 modes :

1. The Guides Editor - Tools->Guides->Edit menu item
1. The Guides Player - Tools->Guides->Play menu item

If you have not yet started work on a Guide, simply select the Edit option and you can start immediately.

## Documentation
Documentation can be found at https://codio.com/docs/ide/tools/guides/

If you find any anomalies, and there will be some due to the many changes we are currently making to Guides, then please email ijobling@codio.com with details and we will get this updated quickly.

